import axios from 'axios';
import {AsyncStorage} from 'react-native';

const getToken = async () => {
    return await AsyncStorage.getItem('userToken');
}

export default {
    get: (url, options = {}) => {
        return new Promise(function(resolve, reject) {
            let opt = Object.assign(options, {
                baseURL: 'http://api-denis.digakusite.com',
                headers: {
                    "Content-Type": "application/json",
                    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/10.0',
                },
                maxRedirects: 5,
                validateStatus: function (status) {
                    return status >= 200 && status < 303; // default
                },
            })
            
            const token = getToken()
            if (token) {
                opt.headers["X-Access-Token"] = token;
            }
            axios.get(url, opt)
            .then((responseJson) => resolve(responseJson))
            .catch((err) => reject(err))
        })
    },

    post: (url, options = {}) => {
        return new Promise(function(resolve, reject) {
            const opt = Object.assign(options, {
                baseURL: 'http://api-denis.digakusite.com/'
            })
            
            const token = getToken()
            if (token) {
                opt.headers["X-Access-Token"] = token;
            }
            axios.post(url, opt)
            .then((responseJson) => resolve(responseJson))
            .catch((err) => reject(err))
        })
    }
}