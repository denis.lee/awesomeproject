import React from 'react';
import { FlatList, StyleSheet, View, Text, Image, Modal, ActivityIndicator, TouchableOpacity, Alert, ScrollView } from 'react-native';
import http from '../util/http';

export default class LinksScreen extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			selectedID: null,
			isModalVisible: false
		}
	}



	_onPressItem = (id) => {
		console.log('onpressItem')
		console.log(id)
		this.setState({selectedID: id, isModalVisible: true})
	}


	static navigationOptions = {
		title: 'Users',
	};



	_hideModal = () => {
		this.setState({isModalVisible: false})
	}

	render() {
		return (
			<View
			style={styles.container}
			>
				<MyList onPressItem={this._onPressItem}></MyList>
				{
					this.state.selectedID != null &&
						<MyModal itemId={this.state.selectedID} modalVisible={this.state.isModalVisible} hideModal={this._hideModal}></MyModal>
				}
			</View>
		)
	}
}

const styles = StyleSheet.create({
		container: {
			height: '100%',
			flex: 1,
			paddingTop: 15,
			backgroundColor: '#fff',
		},
		myList: {
			paddingLeft: 10,
			paddingRight: 10,
		},
		myModal: {
			padding: 10
		}
});


class MyModal extends React.Component {
	constructor(props) {
		super(props);

		this.state =  {
			detailData: null,
		}
	}

	_fetchData = () => {
		console.log("fetch data modal")
		console.info(this.props.itemId)
		if (!this.props.itemId) return

		http.get(`https://reqres.in/api/users/${this.props.itemId}`)
		.then((response) => {
			const newData = response.data
			this.setState({detailData: newData})

		})
		.catch((error) => console.log(error))
	}

	render() {
		const item = this.state.detailData
		console.log("render modal")
		console.log(item)
		const contentModal = (item != null)  ? (
			<View style={styles.myModal}>
				<Image source={{uri: item.data.avatar}} style={{width: '100%', height: 200}} />
				<Text style={{fontWeight: 'bold'}}> { item.data.first_name } </Text>
				<Text style={{fontStyle: 'italic'}}> { item.data.last_name } </Text>
			</View>
		) : (
			<ActivityIndicator size="large" color="#0000ff" />
		)
		return (
			<Modal
				animationType="slide"
				transparent={false}
				visible={this.props.modalVisible}
				onRequestClose={() => {
					this.props.hideModal()
					this.setState({detailData: null})
				}}
				onShow = {this._fetchData}
			>
				{contentModal}					
			</Modal>
		)
	}
}

class MyItem extends React.Component {
	_onPress = () => {
		console.log('onpress')
		console.info(this.props.person.id)
		this.props.onPressItem(this.props.person.id);
	}

	render() {
		const item = this.props.person
		return (
			<TouchableOpacity onPress={this._onPress}>
					<Image source={{uri: item.avatar}} style={{width: '100%', height: 200}} />
					<Text style={{fontWeight: 'bold'}}> { item.first_name } </Text>
					<Text style={{fontStyle: 'italic'}}> { item.last_name } </Text>
			</TouchableOpacity>
		)
	}
}

class MyList extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			refreshing: false,
			data: [],
		},


		this.page = 1
		this.endData = false
	}

	componentDidMount() {
		this._onRefresh()
	}

	_fetchPeopleData(onSuccess) {
		if (this.state.refreshing) return;

		this.setState({refreshing: true})
		http.get('https://reqres.in/api/users', {
			params: {
				page: this.page
			}
		})
		.then((response) => {
			const data = response.data
			this.endData = data.total_pages == data.page
			onSuccess(data, () => {
				this.setState({refreshing: false})
			})
		})
		.catch((error) => console.log(error))
	}

	_onRefresh = () => {
		this.page = 1
		this._fetchPeopleData((response, afterState) => {
			const newData = response.data
			this.setState({
				data: newData
			})
			afterState()
		})
	}

	_onLoadMoreData = () => {
		if (!this.endData) {
			this.page = this.page + 1
			this._fetchPeopleData((response, afterState) => {
				const newData = response.data
				this.setState({
					data: [...this.state.data, ...newData]
				})
				afterState()
			})
		}
	}

	_onPressItem = (id) => {
		console.log('onpressItem')
		console.log(id)
		this.props.onPressItem(id);
	}

	render() {
		return (
			<FlatList 
				refreshing = {this.state.refreshing}
				onRefresh = {this._onRefresh}
				onEndReached = {this._onLoadMoreData}
				onEndReachedThreshold = {0.01}
				contentContainerStyle = {styles.myList}
				data = {this.state.data}
				keyExtractor = {(item, index) => item.id.toString()}
				renderItem = {({item, index}) => (
					<MyItem person={item} onPressItem={this._onPressItem}></MyItem>
				)}
			/>
		);
	}
}