import React from "react";
import { View, Text, Button, AsyncStorage } from "react-native";
import { createStackNavigator, createAppContainer } from "react-navigation";
import DashBoardScreen from './screens/LoggedInScreen/DashBoardScreen';
import http from './util/http';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text>Home Screen 1</Text>
        <Button
          title="Go to Details"
          onPress={() => this.props.navigation.navigate('DashBoard')}
        />
      </View>
    );
  }
}

const AppNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    DashBoard: DashBoardScreen,
  },
  {
    initialRouteName: "Home"
  }
);

export default createAppContainer(AppNavigator);